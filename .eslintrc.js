module.exports = {
  extends: ["prettier"],
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error",
    "max-len": ["error", 120]
  },
  env: {
    browser: true,
    node: true
  },
  parser: "babel-eslint"
};
