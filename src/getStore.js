import ReducerRegistry from "./ReducerRegistry";
import createStore from "./createStore";

let store, reducerRegistry;

if (typeof window !== "undefined") {
  if (typeof window.__globalStore === "undefined") {
    reducerRegistry = new ReducerRegistry();
    window.__globalStore = {
      store: createStore(reducerRegistry),
      reducerRegistry
    };
  }
  store = window.__globalStore.store;
  reducerRegistry = window.__globalStore.reducerRegistry;
} else {
  throw new Error("window is not defined");
}

export { store, reducerRegistry };
