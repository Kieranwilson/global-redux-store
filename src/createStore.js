import { combineReducers, applyMiddleware, createStore, compose } from "redux";
import thunk from "redux-thunk";

const initialState = {};
let store;

// Preserve initial state for not-yet-loaded reducers
const combine = reducers => {
  const reducerNames = Object.keys(reducers);
  Object.keys(initialState).forEach(item => {
    if (reducerNames.indexOf(item) === -1) {
      reducers[item] = (state = null) => state;
    }
  });
  return combineReducers(reducers);
};

export default function configureStore(reducerRegistry) {
  const middleware = [thunk];
  let enhancer;

  if (process && process.env && process.env.NODE_ENV === "development") {
    const { createLogger } = require("redux-logger");

    middleware.push(createLogger());
    const composeEnhancers =
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    enhancer = composeEnhancers(applyMiddleware(...middleware));
  } else {
    enhancer = applyMiddleware(...middleware);
  }

  const reducer = combine(reducerRegistry.getReducers());
  store = createStore(reducer, initialState, enhancer);

  // Replace the store's reducer whenever a new reducer is registered.
  reducerRegistry.setChangeListener(reducers => {
    store.replaceReducer(combine(reducers));
  });

  return store;
}
