class ReducerRegistry {
  constructor() {
    this._emitChange = null;
    this._reducers = {};
    this._count = 0;
  }

  getReducers() {
    return { ...this._reducers };
  }

  registerNamed(name, reducer) {
    this._reducers = { ...this._reducers, [name]: reducer };
    if (this._emitChange) {
      this._emitChange(this.getReducers());
    }
    this._count += 1;
  }

  register(reducer) {
    this.registerNamed(`${this._count}`, reducer);
    return this._count - 1;
  }

  setChangeListener(listener) {
    this._emitChange = listener;
  }
}

export default ReducerRegistry;
