import { reducerRegistry } from "./getStore";
export { store } from "./getStore";
export const registerNamedReducer = (name, reducer) =>
  reducerRegistry.registerNamed(name, reducer);
export const registerReducer = reducer => reducerRegistry.register(reducer);
