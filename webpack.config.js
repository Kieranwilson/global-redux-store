const path = require("path");

module.exports = {
  mode: process.env.NODE_ENV || "production",
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "index.js",
    library: "global-redux-store",
    libraryTarget: "umd",
    globalObject: "this"
  },
  externals: {
    redux: "redux",
    "redux-logger": "redux-logger",
    "redux-thunk": "redux-thunk"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: "babel-loader"
      }
    ]
  }
};
